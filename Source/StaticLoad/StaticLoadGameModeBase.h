// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StaticLoadGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class STATICLOAD_API AStaticLoadGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
